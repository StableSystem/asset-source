echo "starting tile conversion to DDS"

magick convert -define dds:compression=dxt3 -extract 2048x2048+300+2200 grunge_master.png grunge_1.dds
magick convert -define dds:compression=dxt3 -extract 1024x2048+2792+3354 grunge_master.png grunge_2.dds
magick convert -define dds:compression=dxt3 -extract 512x2048+2430+2073 grunge_master.png grunge_3.dds
magick convert -define dds:compression=dxt3 -extract 2048x1024+2048+1074 grunge_master.png grunge_4.dds
magick convert -define dds:compression=dxt3 -extract 512x512+3236+3127 grunge_master.png grunge_5.dds
