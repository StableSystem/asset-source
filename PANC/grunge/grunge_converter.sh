echo "starting grunge cut and conversion"
echo "starting: grunge_1.dds"
magick convert -define dds:compression=dxt5 -extract 4096x4096+3072+0 grunge_overlay.tif grunge_1.dds
echo "finished: grunge_1.dds"
echo "starting: grunge_2.dds"
magick convert -define dds:compression=dxt5 -extract 4096x2048+0+7168 grunge_overlay.tif grunge_2.dds
echo "finished: grunge_2.dds"
echo "starting: grunge_3.dds"
magick convert -define dds:compression=dxt5 -extract 4096x4096+4096+5120 grunge_overlay.tif grunge_3.dds
echo "finished: grunge_3.dds"
echo "starting: grunge_4.dds"
magick convert -define dds:compression=dxt5 -extract 4096x2048+8192+6144 grunge_overlay.tif grunge_4.dds
echo "finished: grunge_4.dds"
echo "starting: grunge_5.dds"
magick convert -define dds:compression=dxt5 -extract 4096x2048+5120+9216 grunge_overlay.tif grunge_5.dds
echo "finished: grunge_5.dds"
echo "starting: grunge_6.dds"
magick convert -define dds:compression=dxt5 -extract 4096x1024+3072+4096 grunge_overlay.tif grunge_6.dds
echo "finished: grunge_6.dds"
echo "starting: grunge_7.dds"
magick convert -define dds:compression=dxt5 -extract 1024x1024+8192+8192 grunge_overlay.tif grunge_7.dds
echo "finished: grunge_7.dds"
echo "finished program"
