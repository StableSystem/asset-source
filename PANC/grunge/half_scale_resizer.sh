echo "starting grunge cut and conversion"
echo "starting: grunge_1.dds"
magick convert grunge_1.dds -resize 2048x2048 resized/grunge_1.dds
echo "finished: grunge_1.dds"
echo "starting: grunge_2.dds"
magick convert grunge_2.dds -resize 2048x1024 resized/grunge_2.dds
echo "finished: grunge_2.dds"
echo "starting: grunge_3.dds"
magick convert grunge_3.dds -resize 2048x2048 resized/grunge_3.dds
echo "finished: grunge_3.dds"
echo "starting: grunge_4.dds"
magick convert grunge_4.dds -resize 2048x1024 resized/grunge_4.dds
echo "finished: grunge_4.dds"
echo "starting: grunge_5.dds"
magick convert grunge_5.dds -resize 2048x1024 resized/grunge_5.dds
echo "finished: grunge_5.dds"
echo "starting: grunge_6.dds"
magick convert grunge_6.dds -resize 2048x512 resized/grunge_6.dds
echo "finished: grunge_6.dds"
echo "starting: grunge_7.dds"
magick convert grunge_7.dds -resize 512x512 resized/grunge_7.dds
echo "finished: grunge_7.dds"
echo "finished program"
