# Asset Source

This is where all the source material is located for all of my xplane scenery projects. Within this repository you can find the following source material:

*  Blender source files
*  Autodesk Inventor source files
*  x-plane obj8 files
*  Baked texture and normals
*  raw diffuse, normal, and metalness textures

To conctact me about this repo please send me a message on [the org](https://forums.x-plane.org/index.php?/profile/534962-function86/) or send me a pm on [reddit](https://old.reddit.com/user/StableSystem/)

This repository and its contents are protected under [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)