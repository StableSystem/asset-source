when working on new textures please use PNG as your export format. PNG is a lossless form of compression and ensures you wont get artifacts that will pass through to the final version

when you are finished working on a new texture, retain the original PNG version in the "png" folder inside each sub directory in case it needs to be edited or derivative textures need to be made. Complete textures should be converted to DDS using the lowest possible form of DXT compression. See this article to learn when to use DXT1, 3, and 5 https://www.fsdeveloper.com/wiki/index.php?title=DXT_compression_explained

For doing batch DDS compression I recommend using DDS converter
https://vvvv.org/contribution/dds-converter